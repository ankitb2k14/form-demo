import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="header-wrapper">
          <nav className="nav nav-pills flex-column flex-sm-row">
            <a className="flex-sm-fill text-sm-center nav-link active" href="javascript:void(0);">Unfall & Beteiligte</a>
            <a className="flex-sm-fill text-sm-center nav-link" href="javascript:void(0);">Fahrzeugauswahl</a>
            <a className="flex-sm-fill text-sm-center nav-link" href="javascript:void(0);">Fahrzeugzustand</a>
            <a className="flex-sm-fill text-sm-center nav-link" href="javascript:void(0);">Fotos</a>
            <a className="flex-sm-fill text-sm-center nav-link" href="javascript:void(0);">Schadenskalkulation</a>
            <a className="flex-sm-fill text-sm-center nav-link" href="javascript:void(0);">Rechnung</a>
            <a className="flex-sm-fill text-sm-center nav-link" href="javascript:void(0);">Druck & Versand</a>
            <a className="flex-sm-fill text-sm-center nav-link" href="javascript:void(0);"><i className="fa fa-ellipsis-v"></i></a>
          </nav>
        </header>
        <div className="content-wrapper">
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <form className="custom-from">
              <h3>Register Form</h3>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">First Name</label>
                  <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Last Name</label>
                  <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputPassword1">Email</label>
                  <input type="email" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputPassword1">Password</label>
                  <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputPassword1">Confirm Password</label>
                  <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                </div>
                <div className="form-group">
                  <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="customCheck1" />
                      <label className="custom-control-label" htmlFor="customCheck1">Check me out</label>
                  </div>
                </div>
                <button type="submit" className="btn btn-outline-primary">Submit</button>
              </form>
            </div>
            <div className="col-md-6">
              <form className="custom-from login-form">
                <h3>Login Form</h3>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Email address</label>
                  <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputPassword1">Password</label>
                  <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                </div>
                <button type="submit" className="btn btn-outline-primary">Submit</button>
              </form>
            </div>
          </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
